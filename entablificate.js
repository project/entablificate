var Entablificate = Entablificate || {settings:{seed: 0}};

(function ($) { // Closure for jQuery.
  /**
   * Turn a panel's panes into tabs using jQueryUI.
   *
   * This takes a panel (or whatever selector you pass in as containerID) and finds all
   * of the panel panes (class: .panel-pane), turning each one into tabs.
   *
   * If you do this in a behavior, make sure to wrap every container with a processed class.
   *
   * @param containerID
   *   THIS SELECTOR MUST IDENTIFY ONE ELEMENT, NEVER MORE. This will treat the one
   *   element as the starting point for finding child panes and turning them into 
   *   tabs.
   */
  Entablificate.tabs = function (containerId) {
    var $topPanel = $(containerId);
    var $toc = $('<ul class="entabilificate-tab-toc"/>');
    $topPanel.find('.panel-pane').each(function (i, e) {
      var $this = $(this);
      var id = 'entablificate-tab-' + Entablificate.settings.seed + '-' + i;
      var title = $this.find('h2.pane-title').text();

      $this.attr('id', id);

      // Build a table of contents for the tabs:
      $toc.append('<li class="entablificate-tab-header"><a href="#' + id + '">' + title + '</a></li>');
      $toc.addClass('tabs');

    }).wrapAll('<div class="entablificate-tabs"></div>').eq(0).before($toc);
    $(containerId +' .entablificate-tab-header').parent().find('li:first>a').addClass('first').end().find('li:last>a').addClass('last');

    var targetSelector = containerId + ' .entablificate-tabs';

    $(targetSelector + '>ul.entabilificate-tab-toc').tabs({});
  
    Entablificate.settings.seed++;
  }
  
  /**
   * Behavior to automatically add tabs to panels that have the specified class.
   */
  Drupal.behaviors.autoentablificate = function () {
    var selector = Drupal.settings.entablificate.selector || '.tabs-panel-pane';
    $(selector).each(function (i, e) {
      var $tabContainer = $(this).parents('.panel-display:first:not(.entablificate-processed)');
      if ($tabContainer.size() > 0) {
        var tid = $(this).parents('.panel-display:first:not(.entablificate-processed)').addClass('entablificate-processed').attr('id');
        Entablificate.tabs('#' + tid);
      }
    });
  }
})(jQuery);